use criterion::{black_box, criterion_group, criterion_main, BenchmarkId, Criterion};
use sym_rs::Expression;

fn criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("expression_parsing");

    let inputs = &[
        ("easy", "1+2*3^4"),
        ("medium", "exp(x)*sin(1+2*3^4)"),
        ("hard", "exp(x)*sin(1+2*3^4)+(x^2+y^2)^0.5"),
    ];

    for i in inputs {
        group.bench_with_input(BenchmarkId::from_parameter(i.0), i.1, |b, i| {
            b.iter(|| Expression::from_str(black_box(i)))
        });
    }
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
