use criterion::{black_box, criterion_group, criterion_main, BenchmarkId, Criterion};
use sym_rs::Expression;
use sym_rs::Rule;

fn criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("expression_matching");

    let inputs = &[
        (
            "easy",
            Expression::from_str("1+2*3^4").unwrap(),
            Rule::from_str("$1+$2=$2+$1").unwrap(),
            // prevent one-line collapse
        ),
        (
            "medium",
            Expression::from_str("a*(1+exp(ln(x^2)))").unwrap(),
            Rule::from_str("exp(ln($1))=$1").unwrap(),
        ),
        (
            "hard",
            Expression::from_str("(e^(i*2*t))^t^2+1+e^(i*4*t)").unwrap(),
            Rule::from_str("e^(i*$1)=cos($1)+i*sin($1)").unwrap(),
        ),
    ];

    for i in inputs {
        group.bench_with_input(BenchmarkId::from_parameter(i.0), i, |b, (_, e, r)| b.iter(|| r.apply(black_box(e))));
    }
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
