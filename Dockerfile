# syntax=docker/dockerfile:1.4
FROM alpine:latest as build-base
RUN apk add --no-cache gcc musl-dev m4 make gmp-dev mpfr-dev mpc1-dev rustup
RUN rustup-init -y --default-toolchain 1.60
ENV PATH="${PATH}:/root/.cargo/bin" CARGO_FEATURE_USE_SYSTEM_LIBS=y
RUN RUSTFLAGS="-C target-feature=-crt-static" cargo install cargo-chef cargo2junit
WORKDIR /mnt/src

FROM build-base as build
ENV RUSTFLAGS="-C target-feature=+crt-static"
COPY Cargo.* ./
RUN cargo chef prepare && cargo chef cook --release
COPY . ./
RUN cargo build --release

FROM build-base as test
COPY Cargo.* ./
RUN cargo chef prepare && cargo chef cook --tests
COPY . ./
RUN cargo test -- -Zunstable-options --format=json | tee test.json && \
    cat test.json | cargo2junit > junit.xml

FROM scratch
COPY --from=build /mnt/src/target/release/sym-rs .
COPY --from=test /mnt/src/junit.xml .