use std::collections::HashMap;
use std::collections::HashSet;
use std::fmt;
use std::fmt::Display;
use std::fmt::Write;
use std::rc::Rc;

use rug::Complex;
use rustyline::error::ReadlineError;
use rustyline::Editor;

use crate::expression::NumericalApproximation;
use crate::rule::Rule;
use crate::Expression;

pub struct Repl {
    editor: Editor<()>,
    rules: Vec<Rule>,
    locals: HashMap<String, Rc<Expression>>,
    last_res: Option<Rc<Expression>>,
}

struct PrettyPrint<T>(T);

impl Display for PrettyPrint<&Complex> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0.real())?;
        if !self.0.imag().is_zero() {
            write!(f, "{:+}i", self.0.imag())?;
        }
        Ok(())
    }
}

impl Repl {
    pub fn new() -> Repl {
        Repl {
            editor: Editor::new(),
            rules: Rule::all(),
            locals: HashMap::new(),
            last_res: None,
        }
    }

    pub fn run(&mut self) -> Result<(), ReadlineError> {
        loop {
            let line = self.editor.readline(">> ")?;
            if !line.is_empty() {
                self.editor.add_history_entry(&line);
            }
            let res = self.eval(line).unwrap();
            println!("{}", res);
        }
    }

    fn eval(&mut self, command: String) -> Result<String, fmt::Error> {
        let mut res = String::new();

        let expr = Expression::from_str(&command).unwrap();
        write!(&mut res, "{}", expr)?;

        if let Some(ref val) = expr.approximate((100, 100)) {
            write!(&mut res, " = {}", PrettyPrint(val))?;
        }

        write!(&mut res, "\n\n")?;
        writeln!(&mut res, "Alternative expressions:")?;

        type Seen = HashSet<Rc<Expression>>;

        fn explore(seen: &mut Seen, rules: &[Rule], s: &mut dyn Write, e: &Expression, indent: u8) -> fmt::Result {
            let children = rules.iter().flat_map(|r| r.apply(e));

            for e in children {
                if !seen.contains(&e) {
                    for _ in 0..indent {
                        write!(s, " ")?;
                    }
                    writeln!(s, "{}", e)?;
                    seen.insert(Rc::new(e.clone()));
                    explore(seen, rules, s, &e, indent + 2)?;
                }
            }

            Ok(())
        }

        explore(&mut Seen::from([Rc::new(expr.clone())]), &self.rules, &mut res, &expr, 0)?;

        Ok(res)
    }
}
