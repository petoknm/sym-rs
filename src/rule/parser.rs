//! Rule parser module.
//!
//! PEG parser using the following grammar:
//! ```
//! sum         ::= product ([+-] product)*
//! product     ::= exponential ([*/] exponential)*
//! exponential ::= factor ("^" factor)*
//! factor      ::= bracketed | placeholder | literal | identifier
//! bracketed   ::= identifier? "(" sum ")"
//! placeholder ::= "$" [0-9]+
//! literal     ::= [0-9]+(.[0-9]*)?
//! identifier  ::= [a-zA-Z][a-zA-Z0-9]*
//! ```

use std::rc::Rc;

use nom::branch::alt;
use nom::character::complete::alpha1;
use nom::character::complete::alphanumeric0;
use nom::character::complete::char;
use nom::character::complete::digit0;
use nom::character::complete::digit1;
use nom::combinator::complete;
use nom::combinator::opt;
use nom::combinator::recognize;
use nom::multi::separated_list1;
use nom::sequence::delimited;
use nom::sequence::tuple;
use nom::IResult;

use super::Pattern;

pub fn pattern_complete(input: &str) -> IResult<&str, Pattern> {
    complete(pattern)(input)
}

pub fn pattern(input: &str) -> IResult<&str, Pattern> {
    sum(input)
}

/// sum ::= product ([+-] product)*
pub fn sum(input: &str) -> IResult<&str, Pattern> {
    let (input, mut elements) = separated_list1(char('+'), product)(input)?;
    let end = elements.pop().unwrap();
    let e = elements
        .drain(..)
        .rev()
        .fold(end, |a, b| Pattern::Addition(Rc::new(b), Rc::new(a)));
    Ok((input, e))
}

/// product ::= exponential ([*/] exponential)*
pub fn product(input: &str) -> IResult<&str, Pattern> {
    let (input, mut elements) = separated_list1(char('*'), exponential)(input)?;
    let end = elements.pop().unwrap();
    let e = elements
        .drain(..)
        .rev()
        .fold(end, |a, b| Pattern::Multiplication(Rc::new(b), Rc::new(a)));
    Ok((input, e))
}

/// exponential ::= factor ("^" factor)*
pub fn exponential(input: &str) -> IResult<&str, Pattern> {
    let (input, mut elements) = separated_list1(char('^'), factor)(input)?;
    let end = elements.pop().unwrap();
    let e = elements.drain(..).rev().fold(end, |a, b| Pattern::Exponentiation {
        base: Rc::new(b),
        exponent: Rc::new(a),
    });
    Ok((input, e))
}

/// factor ::= bracketed | placeholder | literal | identifier
pub fn factor(input: &str) -> IResult<&str, Pattern> {
    alt((bracketed, placeholder, literal, identifier))(input)
}

/// bracketed ::= identifier? "(" sum ")"
pub fn bracketed(input: &str) -> IResult<&str, Pattern> {
    let (input, (mut id, mut e)) = tuple((opt(identifier), delimited(char('('), sum, char(')'))))(input)?;
    if let Some(Pattern::Identifier(i)) = id.take() {
        e = Pattern::Function(i, Rc::new(e))
    }
    Ok((input, e))
}

/// placeholder ::= "$" [0-9]+
pub fn placeholder(input: &str) -> IResult<&str, Pattern> {
    let (input, (_, n)) = tuple((char('$'), digit1))(input)?;
    Ok((input, Pattern::Placeholder(n.parse().unwrap())))
}

/// literal ::= [0-9]+(.[0-9]*)?
pub fn literal(input: &str) -> IResult<&str, Pattern> {
    let (input, l) = recognize(tuple((digit1, opt(tuple((char('.'), digit0))))))(input)?;
    Ok((input, Pattern::Literal(l.to_owned())))
}

/// identifier ::= [a-zA-Z][a-zA-Z0-9]*
pub fn identifier(input: &str) -> IResult<&str, Pattern> {
    let (input, s) = recognize(tuple((alpha1, alphanumeric0)))(input)?;
    Ok((input, Pattern::Identifier(s.to_owned())))
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn placeholder_1() {
        let (_, p) = pattern("1+$1").unwrap();
        assert_eq!(
            p,
            Pattern::Addition(Rc::new(Pattern::Literal("1".into())), Rc::new(Pattern::Placeholder(1)))
        );
    }
}
