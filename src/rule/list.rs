use crate::rule::Rule;

pub fn get_all() -> Vec<Rule> {
    vec![
        Rule::from_str("$1+$2 = $2+$1").unwrap(),
        Rule::from_str("$1*$2 = $2*$1").unwrap(),
        Rule::from_str("$1*($2+$3) = $1*$2+$1*$3").unwrap(),
        Rule::from_str("exp($1) = e^$1").unwrap(),
        Rule::from_str("sin($1)^2+cos($1)^2 = 1").unwrap(),
        Rule::from_str("tan($1) = sin($1)/cos($1)").unwrap(),
        Rule::from_str("cos(2*$1) = 1-2*sin($1)^2").unwrap(),
        Rule::from_str("cos($1)^2 = (cos(2*$1)+1)/2").unwrap(),
        Rule::from_str("e^(i*$1) = cos($1)+i*sin($1)").unwrap(),
    ]
    .drain(..)
    .flat_map(|r| [r.mirror(), r])
    .collect()
}
