use std::collections::HashMap;
use std::rc::Rc;

use crate::Expression;

mod list;
mod parser;

#[derive(Clone)]
pub struct Rule(Rc<Pattern>, Rc<Pattern>);

impl Rule {
    pub fn all() -> Vec<Rule> {
        list::get_all()
    }

    pub fn from_str(s: &str) -> Result<Rule, ()> {
        let (p1, p2) = s.split_once('=').ok_or(())?;
        let p1 = Pattern::from_str(p1.trim())?;
        let p2 = Pattern::from_str(p2.trim())?;
        Ok(Rule(Rc::new(p1), Rc::new(p2)))
    }

    fn mirror(&self) -> Self {
        Rule(self.1.clone(), self.0.clone())
    }

    pub fn apply(&self, e: &Expression) -> Vec<Expression> {
        let m = self.0.matches(e);

        fn make(e: &Expression, me: &Expression, p: &Pattern, m: &HashMap<u8, &Expression>) -> Expression {
            if (e as *const Expression) == (me as *const Expression) {
                p.substitute(m)
            } else {
                e.map(|e| make(e, me, p, m))
            }
        }

        m.iter().map(|(me, m)| make(e, me, &self.1, m)).collect()
    }
}

/// Algebraic expression pattern.
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Pattern {
    Placeholder(u8),
    // Variants of crate::expresson::Expression
    Literal(String),
    Identifier(String),
    Addition(Rc<Pattern>, Rc<Pattern>),
    Subtraction(Rc<Pattern>, Rc<Pattern>),
    Multiplication(Rc<Pattern>, Rc<Pattern>),
    Division(Rc<Pattern>, Rc<Pattern>),
    Exponentiation { base: Rc<Pattern>, exponent: Rc<Pattern> },
    Function(String, Rc<Pattern>),
}

impl Pattern {
    pub fn from_str(s: &str) -> Result<Pattern, ()> {
        let (_, p) = parser::pattern_complete(s).map_err(|_| ())?;
        Ok(p)
    }

    pub fn matches<'a>(&self, expr: &'a Expression) -> Vec<(&'a Expression, HashMap<u8, &'a Expression>)> {
        // Find root of the pattern
        fn find_roots<'a, 'b, 'c>(
            p: &'a Pattern,
            e: &'c Expression,
        ) -> Vec<(&'c Expression, HashMap<u8, &'c Expression>)> {
            match (p, e) {
                (Pattern::Placeholder(..), _) => panic!("Top level pattern should not be a placeholder."),

                // Try to match exactly
                // TODO: append sub expression search if exact match not found
                (Pattern::Literal(..), Expression::Literal(..)) => placeholders_wrapper(p, e),
                (Pattern::Identifier(..), Expression::Identifier(..)) => placeholders_wrapper(p, e),
                (Pattern::Addition(..), Expression::Addition(..)) => placeholders_wrapper(p, e),
                (Pattern::Subtraction(..), Expression::Subtraction(..)) => placeholders_wrapper(p, e),
                (Pattern::Multiplication(..), Expression::Multiplication(..)) => placeholders_wrapper(p, e),
                (Pattern::Division(..), Expression::Division(..)) => placeholders_wrapper(p, e),
                (Pattern::Exponentiation { .. }, Expression::Exponentiation { .. }) => placeholders_wrapper(p, e),
                (Pattern::Function(..), Expression::Function(..)) => placeholders_wrapper(p, e),

                // Try to match subexpressions
                (p, e) => e.into_iter().flat_map(|e| find_roots(p, e)).collect(),
            }
        }

        // Match placeholders
        fn placeholders_wrapper<'a>(
            p: &Pattern,
            e: &'a Expression,
        ) -> Vec<(&'a Expression, HashMap<u8, &'a Expression>)> {
            let mut m = HashMap::new();
            match placeholders(p, e, &mut m) {
                true => vec![(e, m)],
                false => vec![],
            }
        }

        // Match placeholders
        fn placeholders<'a>(p: &Pattern, e: &'a Expression, m: &mut HashMap<u8, &'a Expression>) -> bool {
            match (p, e) {
                (Pattern::Placeholder(n), e) => {
                    if m.contains_key(n) {
                        m[n] == e
                    } else {
                        m.insert(*n, e);
                        true
                    }
                }
                (Pattern::Literal(a), Expression::Literal(b)) => a == b,
                (Pattern::Identifier(a), Expression::Identifier(b)) => a == b,
                (Pattern::Addition(a, b), Expression::Addition(c, d)) => placeholders(a, c, m) && placeholders(b, d, m),
                (Pattern::Subtraction(a, b), Expression::Subtraction(c, d)) => {
                    placeholders(a, c, m) && placeholders(b, d, m)
                }
                (Pattern::Multiplication(a, b), Expression::Multiplication(c, d)) => {
                    placeholders(a, c, m) && placeholders(b, d, m)
                }
                (Pattern::Division(a, b), Expression::Division(c, d)) => placeholders(a, c, m) && placeholders(b, d, m),
                (
                    Pattern::Exponentiation { base: a, exponent: b },
                    Expression::Exponentiation { base: c, exponent: d },
                ) => placeholders(a, c, m) && placeholders(b, d, m),
                (Pattern::Function(a, b), Expression::Function(c, d)) => a == c && placeholders(b, d, m),
                _ => false,
            }
        }

        find_roots(self, expr)
    }

    pub fn substitute(&self, subs: &HashMap<u8, &Expression>) -> Expression {
        match self {
            Pattern::Placeholder(n) => subs[n].clone(),
            Pattern::Literal(a) => Expression::Literal(a.clone()),
            Pattern::Identifier(a) => Expression::Identifier(a.clone()),
            Pattern::Addition(a, b) => Expression::Addition(Rc::new(a.substitute(subs)), Rc::new(b.substitute(subs))),
            Pattern::Subtraction(a, b) => {
                Expression::Subtraction(Rc::new(a.substitute(subs)), Rc::new(b.substitute(subs)))
            }
            Pattern::Multiplication(a, b) => {
                Expression::Multiplication(Rc::new(a.substitute(subs)), Rc::new(b.substitute(subs)))
            }
            Pattern::Division(a, b) => Expression::Division(Rc::new(a.substitute(subs)), Rc::new(b.substitute(subs))),
            Pattern::Exponentiation { base: a, exponent: b } => Expression::Exponentiation {
                base: Rc::new(a.substitute(subs)),
                exponent: Rc::new(b.substitute(subs)),
            },
            Pattern::Function(a, b) => Expression::Function(a.clone(), Rc::new(b.substitute(subs))),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from_str_1() {
        let r = Rule::from_str("e^(i*$1) = cos($1)+i*sin($1)");
        assert!(r.is_ok())
    }

    #[test]
    fn apply_1() {
        let r = Rule::from_str("e^(i*$1) = cos($1)+i*sin($1)").unwrap();
        let e = Expression::from_str("e^(i*pi)").unwrap();
        assert_eq!(r.apply(&e), vec![Expression::from_str("cos(pi)+i*sin(pi)").unwrap()]);
    }

    #[test]
    fn apply_2() {
        let r = Rule::from_str("$1*$1 = $1^2").unwrap();
        let e = Expression::from_str("e^(a*a)").unwrap();
        assert_eq!(r.apply(&e), vec![Expression::from_str("e^a^2").unwrap()]);
    }

    #[test]
    fn pattern_matches_1() {
        let p = Pattern::from_str("e^(i*$1)").unwrap();
        let e = Expression::from_str("e^(i*x)").unwrap();
        assert_eq!(p.matches(&e), vec![(&e, HashMap::from([(1, &Expression::Identifier("x".into()))]))]);
    }

    #[test]
    fn pattern_matches_2() {
        let p = Pattern::from_str("$1*$1").unwrap();
        let e = Expression::from_str("5*5").unwrap();
        assert_eq!(p.matches(&e), vec![(&e, HashMap::from([(1, &Expression::Literal("5".into()))]))]);
    }

    #[test]
    fn pattern_matches_3() {
        let p = Pattern::from_str("$1*$1").unwrap();
        let e = Expression::from_str("5*6").unwrap();
        assert_eq!(p.matches(&e), vec![]);
    }

    #[test]
    fn pattern_matches_4() {
        let p = Pattern::from_str("$1*$1").unwrap();
        let e = Expression::from_str("exp(a*a)").unwrap();
        assert_eq!(
            p.matches(&e),
            vec![(&Expression::from_str("a*a").unwrap(), HashMap::from([(1, &Expression::Identifier("a".into()))]))]
        );
    }
}
