use rug::float::Constant;
use rug::float::Round;
use rug::ops::CompleteRound;
use rug::ops::Pow;
use rug::Complex;

use crate::Expression;

/// This trait provides the ability to numerically approximate expressions.
pub trait NumericalApproximation {
    fn approximate(&self, _precision: (u32, u32)) -> Option<Complex>;
}

impl NumericalApproximation for Expression {
    fn approximate(&self, prec: (u32, u32)) -> Option<Complex> {
        use Expression::*;
        Some(match self {
            Literal(l) => {
                Complex::parse(l)
                    .unwrap()
                    .complete_round(prec, (Round::Nearest, Round::Nearest))
                    .0
            }
            Identifier(i) if i == "e" => Complex::with_val(prec, (1, 0)).exp(),
            Identifier(i) if i == "pi" || i == "π" => Complex::with_val(prec, Constant::Pi),
            Identifier(i) if i == "i" => Complex::with_val(prec, (0, 1)),
            Addition(a, b) => a.approximate(prec)? + b.approximate(prec)?,
            Subtraction(a, b) => a.approximate(prec)? - b.approximate(prec)?,
            Multiplication(a, b) => a.approximate(prec)? * b.approximate(prec)?,
            Division(a, b) => a.approximate(prec)? / b.approximate(prec)?,
            Exponentiation { base, exponent } => base.approximate(prec)?.pow(exponent.approximate(prec)?),
            Function(a, x) if a == "sin" => x.approximate(prec)?.sin(),
            Function(a, x) if a == "cos" => x.approximate(prec)?.cos(),
            Function(a, x) if a == "tan" => x.approximate(prec)?.tan(),
            Function(a, x) if a == "exp" => x.approximate(prec)?.exp(),
            Function(a, x) if a == "ln" => x.approximate(prec)?.ln(),
            Function(a, x) if a == "sqrt" => x.approximate(prec)?.sqrt(),
            _ => return None,
        })
    }
}
