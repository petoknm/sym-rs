//! Expression parser module.
//!
//! PEG parser using the following grammar:
//! ```
//! sum         ::= product ([+-] product)*
//! product     ::= exponential ([*/] exponential)*
//! exponential ::= factor ("^" factor)*
//! factor      ::= bracketed | literal | identifier
//! bracketed   ::= identifier? "(" sum ")"
//! literal     ::= [0-9]+(.[0-9]*)?
//! identifier  ::= [a-zA-Z][a-zA-Z0-9]*
//! ```

use std::rc::Rc;

use nom::branch::alt;
use nom::character::complete::char;
use nom::character::complete::{alpha1, alphanumeric0, digit0, digit1};
use nom::combinator::all_consuming;
use nom::combinator::opt;
use nom::combinator::recognize;
use nom::multi::separated_list1;
use nom::sequence::{delimited, tuple};
use nom::IResult;

use crate::Expression;

// fn ws<'a, O, P: Parser<&'a str, O, nom::error::Error<&'a str>>>(
//     parser: P,
// ) -> impl FnMut(&'a str) -> IResult<&'a str, O>
// where
// {
//     delimited(multispace0, parser, multispace0)
// }

pub fn expression(input: &str) -> IResult<&str, Expression> {
    sum(input)
}

pub fn expression_complete(input: &str) -> IResult<&str, Expression> {
    all_consuming(expression)(input)
}

/// sum ::= product ([+-] product)*
pub fn sum(input: &str) -> IResult<&str, Expression> {
    let (input, mut elements) = separated_list1(char('+'), product)(input)?;
    let end = elements.pop().unwrap();
    let e = elements
        .drain(..)
        .rev()
        .fold(end, |a, b| Expression::Addition(Rc::new(b), Rc::new(a)));
    Ok((input, e))
}

/// product ::= exponential ([*/] exponential)*
pub fn product(input: &str) -> IResult<&str, Expression> {
    let (input, mut elements) = separated_list1(char('*'), exponential)(input)?;
    let end = elements.pop().unwrap();
    let e = elements
        .drain(..)
        .rev()
        .fold(end, |a, b| Expression::Multiplication(Rc::new(b), Rc::new(a)));
    Ok((input, e))
}

/// exponential ::= factor ("^" factor)*
pub fn exponential(input: &str) -> IResult<&str, Expression> {
    let (input, mut elements) = separated_list1(char('^'), factor)(input)?;
    let end = elements.pop().unwrap();
    let e = elements.drain(..).rev().fold(end, |a, b| Expression::Exponentiation {
        base: Rc::new(b),
        exponent: Rc::new(a),
    });
    Ok((input, e))
}

/// factor ::= bracketed | literal | identifier
pub fn factor(input: &str) -> IResult<&str, Expression> {
    alt((bracketed, literal, identifier))(input)
}

/// bracketed ::= identifier? "(" sum ")"
pub fn bracketed(input: &str) -> IResult<&str, Expression> {
    let (input, (mut id, mut e)) = tuple((opt(identifier), delimited(char('('), sum, char(')'))))(input)?;
    if let Some(Expression::Identifier(i)) = id.take() {
        e = Expression::Function(i, Rc::new(e))
    }
    Ok((input, e))
}

/// literal ::= [0-9]+(.[0-9]*)?
pub fn literal(input: &str) -> IResult<&str, Expression> {
    let (input, l) = recognize(tuple((digit1, opt(tuple((char('.'), digit0))))))(input)?;
    Ok((input, Expression::Literal(l.to_owned())))
}

/// identifier ::= [a-zA-Z][a-zA-Z0-9]*
pub fn identifier(input: &str) -> IResult<&str, Expression> {
    let (input, s) = recognize(tuple((alpha1, alphanumeric0)))(input)?;
    Ok((input, Expression::Identifier(s.to_owned())))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::Expression::*;

    #[test]
    fn literal_1() {
        let (_, e) = expression_complete("123").unwrap();
        assert_eq!(e, Literal("123".into()));
    }

    #[test]
    fn literal_2() {
        let (_, e) = expression_complete("123.").unwrap();
        assert_eq!(e, Literal("123.".into()));
    }

    #[test]
    fn literal_3() {
        let (_, e) = expression_complete("123.456").unwrap();
        assert_eq!(e, Literal("123.456".into()));
    }

    #[test]
    fn addition_1() {
        let (_, e) = expression_complete("1+2").unwrap();
        assert_eq!(e, Addition(Rc::new(Literal("1".into())), Rc::new(Literal("2".into())),));
    }

    #[test]
    fn addition_2() {
        let (_, e) = expression_complete("1+2+3+4").unwrap();
        assert_eq!(
            e,
            Addition(
                Rc::new(Literal("1".into())),
                Rc::new(Addition(
                    Rc::new(Literal("2".into())),
                    Rc::new(Addition(
                        Rc::new(Literal("3".into())),
                        Rc::new(Literal("4".into())),
                        // prevent collapse to single line
                    )),
                )),
            )
        );
    }

    #[test]
    fn op_precedence_1() {
        let (_, e) = expression_complete("1+2*3").unwrap();
        assert_eq!(
            e,
            Addition(
                Rc::new(Literal("1".into())),
                Rc::new(Multiplication(
                    Rc::new(Literal("2".into())),
                    Rc::new(Literal("3".into())),
                )),
            )
        );
    }

    #[test]
    fn op_precedence_2() {
        let (_, e) = expression_complete("1*2+3").unwrap();
        assert_eq!(
            e,
            Addition(
                Rc::new(Multiplication(
                    Rc::new(Literal("1".into())),
                    Rc::new(Literal("2".into())),
                )),
                Rc::new(Literal("3".into())),
            )
        );
    }

    #[test]
    fn exponentiation_1() {
        let (_, e) = expression_complete("e^x").unwrap();
        assert_eq!(
            e,
            Exponentiation {
                base: Rc::new(Identifier("e".into())),
                exponent: Rc::new(Identifier("x".into()))
            }
        );
    }

    #[test]
    fn exponentiation_2() {
        let (_, e) = expression_complete("e^x^2").unwrap();
        assert_eq!(
            e,
            Exponentiation {
                base: Rc::new(Identifier("e".into())),
                exponent: Rc::new(Exponentiation {
                    base: Rc::new(Identifier("x".into())),
                    exponent: Rc::new(Literal("2".into()))
                })
            }
        );
    }

    #[test]
    fn function_1() {
        let (_, e) = expression_complete("sin(x)").unwrap();
        assert_eq!(e, Function("sin".into(), Rc::new(Identifier("x".into()))));
    }
}
