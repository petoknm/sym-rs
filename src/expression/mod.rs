use std::fmt::Debug;
use std::fmt::Display;
use std::rc::Rc;

pub mod parser;

mod traits;
pub use traits::*;

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum Expression {
    Literal(String),
    Identifier(String),
    Addition(Rc<Expression>, Rc<Expression>),
    Subtraction(Rc<Expression>, Rc<Expression>),
    Multiplication(Rc<Expression>, Rc<Expression>),
    Division(Rc<Expression>, Rc<Expression>),
    Exponentiation { base: Rc<Expression>, exponent: Rc<Expression> },
    Function(String, Rc<Expression>),
}

impl Display for Expression {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        use Expression::*;
        match self {
            Literal(l) => write!(f, "{}", l),
            Identifier(i) => write!(f, "{}", i),
            Addition(a, b) => write!(f, "{} + {}", a, b),
            Subtraction(a, b) => write!(f, "{} - {}", a, b),
            Multiplication(a, b) => write!(f, "{} * {}", a, b),
            Division(a, b) => write!(f, "{} / {}", a, b),
            Exponentiation { base, exponent } => write!(f, "{} ^ {}", base, exponent),
            Function(a, b) => write!(f, "{}({})", a, b),
        }
    }
}

impl<'a> IntoIterator for &'a Expression {
    type Item = &'a Expression;
    type IntoIter = std::vec::IntoIter<Self::Item>;
    fn into_iter(self) -> Self::IntoIter {
        use Expression::*;
        match self {
            Addition(a, b) => vec![a.as_ref(), b.as_ref()].into_iter(),
            Subtraction(a, b) => vec![a.as_ref(), b.as_ref()].into_iter(),
            Multiplication(a, b) => vec![a.as_ref(), b.as_ref()].into_iter(),
            Division(a, b) => vec![a.as_ref(), b.as_ref()].into_iter(),
            Exponentiation { base, exponent } => vec![base.as_ref(), exponent.as_ref()].into_iter(),
            Function(_, b) => vec![b.as_ref()].into_iter(),
            _ => vec![].into_iter(),
        }
    }
}

impl Expression {
    pub fn from_str(s: &str) -> Result<Expression, ()> {
        let (_, e) = parser::expression_complete(s).map_err(|_| ())?;
        Ok(e)
    }

    pub fn map<F: Fn(&Expression) -> Expression>(&self, f: F) -> Expression {
        use Expression::*;
        match self {
            Addition(a, b) => Addition(Rc::new(f(a)), Rc::new(f(b))),
            Subtraction(a, b) => Subtraction(Rc::new(f(a)), Rc::new(f(b))),
            Multiplication(a, b) => Multiplication(Rc::new(f(a)), Rc::new(f(b))),
            Division(a, b) => Division(Rc::new(f(a)), Rc::new(f(b))),
            Exponentiation { base: a, exponent: b } => Exponentiation {
                base: Rc::new(f(a)),
                exponent: Rc::new(f(b)),
            },
            Function(a, b) => Function(a.clone(), Rc::new(f(b))),
            e => e.clone(),
        }
    }
}
