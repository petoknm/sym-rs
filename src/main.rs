#![allow(dead_code)]

mod expression;
pub use expression::Expression;

mod rule;

mod repl;
use repl::Repl;

fn main() {
    let mut repl = Repl::new();
    repl.run().unwrap();
}
