# sym-rs

REPL for interactive symbolic algebra manipulation

## To Do
- [x] syntactic expression equality check
- [x] expression pattern matching
- [x] substitution rule list (equivalance rule list, but from both directions)
- [x] fallible numeric eval
- [x] generate a list of alternative expressions
- [ ] generate a tree of alternative expressions

## Notes

To optimize data locality and remove lots of small allocations, we should use some simple memory pool allocator and instead of having expression nodes carry `std::rc::Rc` to subexpressions, they will just have an index into this pool.

This memory pool would either be global/spanning multiple expressions or would be created on a per expression basis.

Per expression memory pool:
 - could use smaller indices
 - doesn't need GC

Larger memory pool for multiple expressions:
 - larger indices (maybe including generation)
 - needs GC


## Memory pool crates:
| feature                 | slab    | bumpalo | typed-arena | id-arena         | generational-arena |
| -------                 | ----    | ------- | ----------- | --------         | ------------------ |
| can hold multiple types | no      | yes     | no          | no               |                    |
| element deletion        | yes     | no      | no          | no               | yes                |
| key type                | `usize` | `&V`    | `&V`        | `usize+arena_id` | `usize+generation` |
| only safe rust          |         |         |             |                  | yes                |